# Dockerfile image #

## PHP 5.6.34 (Apache) with unzip, Composer, PHPUnit ##

Docker Hub image built and hosted here: https://hub.docker.com/r/jackfellows/php-5.6.34-apache-apt-get-install-unzip/

For use in pipelines: `image: jackfellows/php-5.6.34-apache-apt-get-install-unzip`
